#!/usr/bin/env python

import gitlab
import sys

groupfile='group.md'

def get_token( filename='token.txt'):
	with open(filename) as f:
		token=f.readline()
	return token.strip()

def get_groups_filename( default_groups_filename='groups.txt' ):
	if len(sys.argv) > 1:
		return sys.argv[1]

	return default_groups_filename

def fetch_gl_issues( gl_project, label='deliverable' ):
	return gl_project.issues.list(labels=[label])

if __name__ == "__main__":

	token = get_token()

	gl = gitlab.Gitlab('https://gitlab.com', private_token=token )
	gl.auth()

	list_of_groups_filename = get_groups_filename()

	#groups_deliverable = []

	with open(list_of_groups_filename) as gr_lst:

		for p_name in gr_lst.readlines():
			try:
				project = gl.projects.get(p_name.strip())
				print( "Group: {}".format(project.name_with_namespace.encode('utf-8')))

				print( "- issues: {}/issues".format(project.web_url))
				#group_del = {'issue_link': project.web_url,
				#			'opened_issues': [],
				#			'closed_issues': []}

				issues = fetch_gl_issues( project )
				if len(issues) == 0:
					print "- no issues matching"
				else:
					for issue in issues:
						#print( "- {}: {} ({})".format( issue.title, issue.state. issue.labels) )
						print("- {}".format(issue.title.encode('utf-8') ))
						print("  {} ({})".format(issue.state.encode('utf-8'),
											     ", ".join(issue.labels)))
						#print issue
						#if issue.state == u'opened':
						#	group_del['opened_issues'].append( issue.title )
						#elif issue.state == u'closed':
						#	group_del['closed_issues'].append( issue.title )
						#else:
						#	raise ValueError( 'State >{}< is unknown'.format(issue.format))

						#group_del
						#break
				#groups_deliverable.append( [group_del])

			except gitlab.exceptions.GitlabGetError:
				print("Unknown group")
				print("----------------" )
				print("Unable to fetch file {} from {}".format(groupfile, p_name.strip() ) )
				print( "")
			except TypeError:
				#yes silently failing ...
				pass

		#print(groups_deliverable)
