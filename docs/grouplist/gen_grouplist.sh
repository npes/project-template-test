#!/usr/bin/env bash

set -e
cat grouplist_prepend.md

if ! ../../scripts/gitlab/fetch_groups.py ../../groups.txt; then
  exit $?
fi

cat grouplist_append.md
