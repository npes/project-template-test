---
title: 'COURSE TITLE'
subtitle: 'COURSE SUB TITLE'
authors: ['NAME \<MAIL@ucl.dk\>', 'NAME \<MAIL@ucl.dk\>']
main_author: 'ÆØÅ æøå'
date: \today
email: 'MAIL@ucl.dk'
left-header: \today
right-header: 'COURSE TITLE, weekly plans'
---


Introduction
====================

ÆØÅ æøå

This document is a collection of weekly plans. It is based on the wekly plans in the administrative repository, and is updated automatically on change.

The sections describe the goals and programm for each week of the COURSE TITLE.